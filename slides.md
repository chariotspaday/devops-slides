# DevOps / CI with JavaScript

### Delivering somewhat continuously

Note: entry level talk about what is devops and what it means to engineering

* Real world examples follow



# Your friendly presenter (again)


Jeffrey Labonski

jlabonski@chariotsolutions.com

Consultant, Chariot Solutions

_Hardcore devops believer_ <!-- .element: class="fragment" -->

Note: Once you pop, you can't stop. It's a refreshing change for the better.



# Devops is chronically misunderstood

### So diluted it's almost homeopathic by now



![Devops meeting](/images/meeting.jpg) <!-- .element: height="600" -->

Note: It seems that as soon as sales & marketing gets their hands on a new
buzzword it gets repeated to the point of being meaningless. For post people, it
means "scripting and system administration". See also SOA, Cloud, blockchain,
etc...



# What it is

#### (My Understanding of Truth, by Jeff Labonski, Grade 9)

* About People <!-- .element: class="fragment" -->
* About Processes <!-- .element: class="fragment" -->
* A Philosophy <!-- .element: class="fragment" -->


Note: This is a funamentally people problem- ops hates dev. Dev doesn't trouble
itself with ops. DBAs hate both. Security swoops in and causes chaos. Testers weep.

People: collapse teams, work together. Engineering, ops, dba at the same team.

Process: Continuously improving. Automating. Removing sources of errors. Learning.
Reduce steps and manual touches. Testing!

Philosophy:
Improve. Communicate. Deliver better, faster. Learn without fear of failure.
_Take Ownership_.



# The Blameless Postmortem


Note: the blameless postmortem is the endgame. Nobody is fired, even on large
failures. Processes are reviewed, lessons are shared.

"I failed and I learned the following"
"We failed and will not do so again because"

Reduction of CYA is good for all involved



# What it is not

* A nice title for SAs like "Site Reliability Engineer"
* Magic hidden inside TOGAF or other large cert organizations
* Purchasable
* Easy
* Implementable


Note: Can't implement it. You must change your process. This is organizational.



![Zen](/images/zen2.jpg) <!-- .element: height="600" -->

Note: Change comes from within.



# Of course, software is what enables it to occur


Note: we're particularly blessed by the fact that building and rebuilding
software essentially has zero cost. Unless most industries where you'd see
accountets get angry and yell terms like CAPEX and COGS at you, here's it's
free. It just costs time (which is a simplification, but it's cheaper than
materials _and_ time).

Also, we honestly have no idea how to build software as a species.



# The journey takes a while

Note: Before CD comes CQ



# BUY SEVERAL DEVOPS TODAY! *

\* This statement has not been evaluated by the Software and Servers
Administration. This product is not intended to diagnose, treat, cure, or
prevent any project failures.



# Let's actually do things!


Note: While it's nice to discuss the philosophy, let's work on some examples of
process improvement with tangible results.


---


# Can't trust developers

### They are all lazy and write bad code <!-- .element: class="fragment" -->

### Let's work to prevent that <!-- .element: class="fragment" -->



# Leverage toolchains

* Tighten compilation
    * tsconfig.json
    * react proptypes
    * babel / minify



# Leverage toolchains more

* Lint, lint, lint
    * tslint
    * eslint
    * package-json-verifier <!-- .element: class="fragment" -->
    * npm audit <!-- .element: class="fragment" -->
    * snyk <!-- .element: class="fragment" -->
    * markdownlint <!-- .element: class="fragment" -->
    * tsqlint <!-- .element: class="fragment" -->


Note: everythingalintlint




# Don't never not stop adding quality checks

### They are better than you at this


Note:

* sonar, etc. You can extend existing ones somewhat easily when you encounter
   things.
* Additional programs here are your friend- write your own
* Don't let the language stop you. Got a tool in Java? Run it from scripts!
    * npm jdeploy!




```javascript
scripts: {
    "snyk:test": "snyk test",
    "snyk:protect": "snyk protect",
    "lint": "standardx --verbose",
    "lint:fix": "standardx --verbose --fix",
    "lint:sql": "tsqllint sql/*.sql",
    "validate-package": "pjv -w -r",
    "check-node": "check-node-version --package",
    "test": "cross-env NODE_ENV=unit nyc --reporter=lcov mocha --recursive --exit test/unit",
    "coverage:report": "nyc report",
    "coverage:test": "nyc check-coverage",
    ...
```



```javascript
scripts: {
    "preflight": "npm run snyk:test && npm run lint && npm run lint:sql && npm run ...",

```


Note: give the devs a way of running everything bundled locally.



# Prevent engineering from doing bad things to the repository

### Quality Gates <!-- .element: class="fragment" -->


Note: Implementing quality gates at the repo level via CD

These can be tweaked to suit your team / methodology / lifestyle / inherent
sense of being evil and smug



# Bitbucket Pipelines


Note: Not endorsing a product, just something I have a rather large amount of
experience with. I've also done this in Jenkins. CircleCI and TravisCI also work.

This isn't bad as it's easily integrated into Jira.



```yml
image:
    name: node:stable

pipelines:
    default:
        - step:
            name: "Lint, build, test"
            caches:
                - node
            script:
                - 'npm install'
                - 'npm run validate-package'
                - 'npm run lint'
                - 'npm run lint:sql'
                - 'npm test'
                - 'npm run nyc:coverage:test'
```



![Branch Permissions in Bitbucket](images/branch_perms.png)



![Pipeline overview](images/pipeline_overview.png)



![Pipeline Filure](images/pipeline_failure.png)



![Pipeline email](images/pipeline_email.png)


---


# Can't trust ops

### They are all lazy and can't deploy software <!-- .element: class="fragment" -->



# Build artifacts from code, automatically



```sh
npm run build:prod && tar -cvzf angular-dist.tar.gz dist
scp angular-dist.tar.gz qaserver:/angular

ssh qaserver
cd /angular
rm -rf dist && tar -xvzf angular-dist.tar.gz
```

Note:
* "Move the end of sprint to the customer QA server"
* Ugh, hope I don't miss a step
* Hope I can back out if something goes wrong
* Did they tag that right? Is it on the same commit?
* Hope nobody updated node
* Did I run npn install right?

* This gets weird for nodejs *



```yml
pipelines:
    default:
        - step:
            name: "Lint, build, test"
            caches:
                - node
            artifacts:
                - '*.tar.gz'
            script:
                - 'npm install'
                - 'npm run lint'
                - 'npm test'
                - 'npm run build:prod'
                - 'tar -cvzf angular-dist.tar.gz --totals dist'
```



# Actions across branches



```yml
branches:
    develop:
        - step:
            name: "Develop: lint, build, test, publish snapshot"
            caches:
                - node
            artifacts:
                - '*.tar.gz'
            script:
                - 'npm install'
                - 'npm run validate-package'
                - 'npm run lint'
                - 'npm test'
                - 'npm pack'
                - 'npm publish'
```



![SSH in Bitbucket](images/pipelines_ssh.png)



```yml
artifacts:
    - '*.tar.gz'
script:
    - 'npm install'
    - 'npm build:prod'
    - 'tar -cvzf angular-dist.tar.gz --totals dist'
    - 'scp *.tar.gz "${REMOTE_SSH}"'
    - 'ssh angular@qaserver /usr/local/bin/update_package.sh'
```



![Artifactory](images/artifactory.png)



# Down the rabbit hole: Docker



# Benefits
* Single, atomic deployment unit. <!-- .element: class="fragment" -->
* No dependencies <!-- .element: class="fragment" -->
* Blazingly fast <!-- .element: class="fragment" -->
* Run prod from home <!-- .element: class="fragment" -->
* It's very cool <!-- .element: class="fragment" -->



```
FROM node:10

RUN mkdir /node
RUN chown node.node /node
USER node
COPY --chown=node:node . /node
WORKDIR /node

# Optional- careful!
RUN npm rebuild --update-binary

ENTRYPOINT [ "node", "--use_strict" ]
```



```
FROM nginx:stable

COPY docker/nginx.conf /etc/nginx/nginx.conf
RUN mkdir /static
WORKDIR /static

COPY dist /static

CMD [ '/usr/sbin/nginx' ]
```



```yml
image:
    name: node:10

options:
    docker: true

pipelines:
    default:
        - step:
            - 'npm run build:prod'
            - 'docker login -u "${DOCKER_HUB_USER}" -p "${DOCKER_HUB_PASSWORD}"'
            - 'docker build --pull -t chariotspa/server:develop .'
            - 'docker push chariotspa/server:develop'
```



![Pipeline generating docker](images/pipeline_success.png)


Note:

* erect the entire stack, server, db, front end for test
* Drive the whole thing with selenium for true integration testing


---


# Takeaway (whew!)
* Quality gates
* Automate your builds & deployment
* Configuration as code
    * ECR / EBS / EC2 / ELB / RDS / ETC <!-- .element: class="fragment" -->
    * Cloudformation / Terraform / GDM <!-- .element: class="fragment" -->
    * Kubernetes <!-- .element: class="fragment" -->
    * ServiceNow / vmWare vRa & vRo <!-- .element: class="fragment" -->
    * Wherever the cloud takes you <!-- .element: class="fragment" -->



# Never stop improving
